'''
Object oriented program to implement a linked list. 
The Linked list class Contains the following member functions.
 -> Print the list
 -> Insert an element at beginning position
Example
-------
>>> 
@author saranya
'''


class Node:
    '''create a node for the elements in the list'''

    def __init__(self, dataval=None):
        self.dataval = dataval
        self.nextval = None


class SLinkedList:
    '''class to create a linkedlist'''

    def __init__(self):
        self.headval = None


# Print the linked list

    def listprint(self):
        '''to print a linkedlist'''
        printval = self.headval
        while printval is not None:
            print(printval.dataval, end='->')
            printval = printval.nextval

    def atbegining(self, newdata):
        '''print elements at the beginning'''
        newnode = Node(newdata)
        newnode.nextval = self.headval
        self.headval = newnode
new = SLinkedList()
new.headval = Node("Mon")
e2 = Node("Tue")
e3 = Node("Wed")
new.headval.nextval = e2
e2.nextval = e3
new.atbegining("Sun")
new.listprint()
